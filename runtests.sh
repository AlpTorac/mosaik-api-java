#!/bin/bash
VENV="$(which virtualenv)"
PYTHON="$(which python3)"
ENVDIR="tests/env"
REQUIREMENTS="pytest simpy.io mosaik"
TESTS="tests/"

# Build dist
ant
if [[ $? -ne 0 ]]; then
    exit 1
fi

# Create and update virtualenv
if [ ! -d "${ENVDIR}" ]; then
    echo "Creating virtualenv"
    $VENV -p $PYTHON $ENVDIR
fi
${ENVDIR}/bin/pip install -U $REQUIREMENTS

# Run tests
${ENVDIR}/bin/py.test --ignore="${ENVDIR}" $1 $TESTS
